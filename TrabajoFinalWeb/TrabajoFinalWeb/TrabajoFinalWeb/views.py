'''
Created on Oct 14, 2017

@author: JC
'''
import os
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from ctypes import *

def index(request):
    return render(request,'index.html')

@csrf_exempt
def registrarServicio(request):
    
    CODIGO_TAXISTA_DEFECTO = 4555
    TIPO_SERVICIO_DEFECTO = 1
    FECHA_VACIA = "00/00/0000 00:00:00".encode('utf-8')
    rutaDir = os.path.dirname(os.path.abspath(__file__))
    
    nombreArchivo = (rutaDir+"/../Data/servicio.txt").encode('utf-8')
    libreriaCpp = cdll.LoadLibrary(rutaDir+"/../Lib/libtrabajo-final-libreria.dll")
    
    nombre = request.POST.get("nombre").encode('utf-8')
    origen = request.POST.get("origen").encode('utf-8')
    destino = request.POST.get("destino").encode('utf-8')
    inicio = request.POST.get("inicio").encode('utf-8')
    fin = request.POST.get("fin").encode('utf-8')
    tarifa =c_float(float(request.POST.get("tarifa")))
    
    libreriaCpp.guardarServicioTaxi(CODIGO_TAXISTA_DEFECTO,nombre,origen,destino,TIPO_SERVICIO_DEFECTO,
                                    inicio,fin,FECHA_VACIA,tarifa,nombreArchivo)
        
    return render(request,'confirmacion.html')


