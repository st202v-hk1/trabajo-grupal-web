/*
 * libreria.cpp
 *
 *  Created on: Nov 7, 2017
 *      Author: JC
 */


#include "servicio/servicio.h"
// Estamos utilizando extern 'C'
#include "libreria.h"
#include <iostream>

#include <cstring>

void guardarServicioTaxi(int codConductor, char *nombre,  char *direccionIni, char *direccionDest, int tipoServ,
		char *fechaHoraIni, char *fechaHoraFin, char *fechaHoraReserva, float tarifa, char* nombreArchivo){

	cout<<"Ejecutando codigo C++"<<endl;

	ServicioTaxi servicio;

	servicio.inicializar(codConductor, string(nombre), string(direccionIni), string(direccionDest),
			tipoServ, string(fechaHoraIni), string(fechaHoraFin), string(fechaHoraReserva), tarifa);

	servicio.guardar(string(nombreArchivo));
}


