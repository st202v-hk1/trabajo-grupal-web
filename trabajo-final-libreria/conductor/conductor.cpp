#include <iostream>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <fstream>
#include "../utilitarios/fecha-hora/fecha-hora.h"
#include "../utilitarios/utilitarios.h"
#include "../servicio/servicio.h"
#include "conductor.h"


using namespace std;


void editarDatosPersonales(){
	cout<<"Datos del usuario"<<endl;
	cout<<"******************"<<endl;
	cout<<"Nombre: Juan"<<endl;
	cout<<"Apellido: Perez"<<endl;
	cout<<"DNI: 10258956"<<endl;
	cout<<"Fecha de Nacimiento: 13/04/1982"<<endl;

	cout<<"Ingrese nuevo valor de Nombre:"<<endl;
	leerDatoPrototipo();
	cout<<"Ingrese nuevo valor de Apellido:"<<endl;
	leerDatoPrototipo();
	cout<<"Ingrese nuevo valor de DNI:"<<endl;
	leerDatoPrototipo();
	cout<<"Ingrese nuevo valor de Fecha de Nacimiento:"<<endl;
	leerDatoPrototipo();

	cout<<"Datos almacenados correctamente... "<<endl;

}

void editarInformacionPago(){
	cout<<"Informacion de Pago"<<endl;
	cout<<"********************"<<endl;
	cout<<"Numero de cuenta: xxx - xxxxx -xxxx 9788"<<endl;
	cout<<"Frecuencia de Pago: MENSUAL"<<endl;

	cout<<"Ingrese nuevo valor de Numero de Cuenta:"<<endl;
	leerDatoPrototipo();
	cout<<"Ingrese nuevo valor de Frecuencia de Pago:"<<endl;
	cout<<"Debe seleccionar: 1=SEMANAL, 2=MENSUAL"<<endl;
	leerDatoPrototipo();
	cout<<"Datos almacenados correctamente... "<<endl;

}

void registrarSercicioEstandar(){

	int codConductor=CODIGO_CONDUCTOR_DEFECTO, tipoServ=TIPO_SERVICIO_ESTANDAR;
	float tarifa;
	string nombre, direccionIni, direccionDest, fechaHoraIni, fechaHoraFin, fechaHoraReserva=FECHA_HORA_VACIA;

	ServicioTaxi servicio;

	cout<<"Registro de Servicio Estandar"<<endl;
	cout<<"*****************************"<<endl;
	cout<<"Ingrese nombre y apellido del cliente: "<<endl;
	getline(cin,nombre);

	cout<<"Ingrese direccion de inicio: "<<endl;
	getline(cin,direccionIni);

	cout<<"Ingrese direccion de destino: "<<endl;
	getline(cin,direccionDest);

	cout<<"Ingrese la fecha/hora de inicio del servicio: "<<endl;
	getline(cin,fechaHoraIni);

	cout<<"Ingrese la hora/hora de fin del servicio: "<<endl;
	getline(cin,fechaHoraFin);

	cout<<"Ingrese la tarifa total: "<<endl;
	cin>>tarifa;

	servicio.inicializar(codConductor, nombre, direccionIni, direccionDest, tipoServ,
			fechaHoraIni, fechaHoraFin, fechaHoraReserva, tarifa);

	servicio.guardar();

}

void registrarServicioReserva(){
	cout<<"Registro de Servicio con Reserva"<<endl;
	cout<<"*****************************"<<endl;
	cout<<"Ingrese nombre y apellido del cliente: "<<endl;
	leerDatoPrototipo();
	cout<<"Ingrese direccion de inicio: "<<endl;
	leerDatoPrototipo();
	cout<<"Ingrese direccion de destino: "<<endl;
	leerDatoPrototipo();

	cout<<"Ingrese la fecha y hora de la reserva: "<<endl;
	leerDatoPrototipo();

	cout<<"Ingrese la fecha y hora de inicio del servicio: "<<endl;
	leerDatoPrototipo();

	cout<<"Ingrese la hora y hora de fin del servicio: "<<endl;
	leerDatoPrototipo();

	cout<<"Ingrese la tarifa total: "<<endl;
	leerDatoPrototipo();

	cout<<"Servicio registrado correctamente... "<<endl;
}

void mostrarServiciosUltimoMes(){
	cout<<"Servicios del Ultimo Mes"<<endl;
	cout<<"*************************"<<endl;
	cout<<"Cliente: Pedro Juarez"<<endl;
	cout<<"Punto Inicio: Av. Brasil 355"<<endl;
	cout<<"Fecha/Hora Inicio: 12/06/2017 8:00:05"<<endl;
	cout<<"Punto Destino: Av. Arica 688"<<endl;
	cout<<"Fecha/Hora Fin: 12/06/2017 8:22:08"<<endl;
	cout<<"Tarifa Total: 10.35"<<endl;

}

void buscarServicios(){
	cout<<"Busqueda"<<endl;
	cout<<"*********"<<endl;
	cout<<"Ingrese el rango de fechas para la busqueda>>>>"<<endl;

	cout<<"Fecha Inicio: "<<endl;
	leerDatoPrototipo();
	cout<<"Fecha Fin: "<<endl;
	leerDatoPrototipo();

	cout<<"Resultados de Busqueda"<<endl;
	cout<<"*************************"<<endl;
	cout<<"Cliente: Pedro Juarez"<<endl;
	cout<<"Punto Inicio: Av. Brasil 355"<<endl;
	cout<<"Fecha/Hora Inicio: 12/06/2017 8:00:05"<<endl;
	cout<<"Punto Destino: Av. Arica 688"<<endl;
	cout<<"Fecha/Hora Fin: 12/06/2017 8:22:08"<<endl;
	cout<<"Tarifa Total: 10.35"<<endl;

}

//TODO Borrar
void testGuardar(){

	ServicioTaxi servicio;

	servicio.inicializar(54321,"Juan Perez", "Av. Brasil 355", "Av. Arequipa 620", 1,"12/10/2017 15:35:22", "12/10/2017 16:00:03",
			"00/00/0000 00:00:00",13.55);

	servicio.guardar();
}
