/*
 * conductor.h
 *
 *  Created on: Jun 16, 2017
 *      Author: JC
 */

#ifndef CONDUCTOR_H_
#define CONDUCTOR_H_

#include <iostream>
#include <string>
#include "../utilitarios/utilitarios.h"
#include "../utilitarios/fecha-hora/fecha-hora.h"
#include <fstream>

using namespace std;

const int CODIGO_CONDUCTOR_DEFECTO = 5485;
const int TIPO_SERVICIO_ESTANDAR = 1;


void editarDatosPersonales();
void editarInformacionPago();
void registrarSercicioEstandar();
void registrarServicioReserva();
void mostrarServiciosUltimoMes();
void buscarServicios();
void guardarServicioTaxi(int codConductor, string nombre, string direccionIni, string direccionDest, int tipoServ,
		string fechaHoraIni, string fechaHoraFin, string fechaHoraReserva, float tarifa);

//TODO Borrar
void testGuardar();

#endif /* CONDUCTOR_H_ */
