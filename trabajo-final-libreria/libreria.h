/*
 * libreria.h
 *
 *  Created on: Nov 7, 2017
 *      Author: JC
 */

#ifndef LIBRERIA_H_
#define LIBRERIA_H_



extern "C"{

	void guardarServicioTaxi(int codConductor, char *nombre,  char *direccionIni, char *direccionDest, int tipoServ,
			char *fechaHoraIni, char *fechaHoraFin, char *fechaHoraReserva, float tarifa, char* nombreArchivo);

}



#endif /* LIBRERIA_H_ */
