#include <iostream>
#include <cmath>
#include <vector>
#include "../conductor/conductor.h"


using namespace std;

string tmp;

// Funcion para simular la lectura de datos. Solo para prototipado
void leerDatoPrototipo(bool espacios){
	getline(cin,tmp);
	if(espacios) cin.ignore();
}

// Funcion utilitaria para dividir una cadena en palabras o "tokens" utilizando un caracter de separacion
// TODO: Probar para distintos valores

vector<string> dividir(string cadena, char caracterSeparacion){
	vector<string> respuesta;
	string aux = "";
	for(int i=0;i<cadena.length();i++){
		if(cadena[i]!=caracterSeparacion){
			aux+=cadena[i];
		}else{
			respuesta.push_back(aux);
			aux="";
		}
	}
	if(aux!="") respuesta.push_back(aux);
	return respuesta;
}

// Funcion para interpretar la opcion ingresada por el usuario. En caso el valor no sea numerico se devuelve -1
int convertirDigitos(string s){
	int valor = 0;
	int respuesta = 0;
	for(int i=0;i<s.length();i++){
		valor = s[i] - '0';
		if(valor>=0 && valor<=9){
			//cout<<respuesta<<endl;
			respuesta+=pow(10,s.length()-i-1)*valor;
		}else{
			return -1;
		}
	}
	return respuesta;
}

// Completa el numero, hasta llegar a la cantidad de digitos especificada, utilizando ceros hacia la izquierda
// En caso el numero tenga mayor cantidad de digitos que lo indicado, realiza la conversion a cadena directamente
string completarConCeros (int numero, int cantidadDigitos){

	int max = log10(numero)+1;
	if(max<cantidadDigitos){
		max=cantidadDigitos;
	}
	string respuesta(" ",max);
	for(int i=max-1;i>=0;i--){
		respuesta[i] = numero%10+'0';
		numero/=10;
	}
	return respuesta;
}
