/*
 * fecha-hora.cpp
 *
 *  Created on: Nov 7, 2017
 *      Author: JC
 */


#include "fecha-hora.h"

void Fecha::inicializar(string cadena){
	dia = convertirDigitos(cadena.substr(0,2));
	mes = convertirDigitos(cadena.substr(3,2));
	anio = convertirDigitos(cadena.substr(6,4));
}
// Obtiene la representacion de la fecha como cadena, en formato dd/mm/aaaa
string Fecha::convertirCadena(){
	stringstream ss;
	ss<<completarConCeros(dia,2)<<"/"<<completarConCeros(mes,2)<<"/"<<completarConCeros(anio,4);
	return ss.str();
}


void Hora::inicializar(string cadena){
	horas = convertirDigitos(cadena.substr(0,2));
	minutos = convertirDigitos(cadena.substr(3,2));
	segundos = convertirDigitos(cadena.substr(6,2));
}
string Hora::convertirCadena(){
	stringstream ss;
	ss<<completarConCeros(horas,2)<<":"<<completarConCeros(minutos,2)<<":"
			<<completarConCeros(segundos,2);
	return ss.str();
}

