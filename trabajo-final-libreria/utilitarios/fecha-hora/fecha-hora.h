#ifndef FECHA_HORA_H_
#define FECHA_HORA_H_

#include "../utilitarios.h"
#include <sstream>

const string FECHA_HORA_VACIA = "00/00/0000 00:00:00";

// Estructura que representa una fecha para manipulacion y operaciones
struct Fecha{
	int dia;
	int mes;
	int anio;
	// Permite inicializar la estructura dada una cadena de formato dd/mm/aaaa
	void inicializar(string cadena);
	// Obtiene la representacion de la fecha como cadena, en formato dd/mm/aaaa
	string convertirCadena();
};

// Estructura que representa una hora para manipulacion y operaciones
struct Hora{
	int horas;
	int minutos;
	int segundos;
	// Permite inicializar la estructura dada una cadena de formato hh:mm:ss
	void inicializar(string cadena);
	// Obtiene la respresentacion de la estructura como cadena hh:mm:ss
	string convertirCadena();
};

#endif //FECHA_HORA_H_
