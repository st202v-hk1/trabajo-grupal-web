/*
 * menu.h
 *
 *  Created on: Jun 16, 2017
 *      Author: JC
 */

#ifndef UTILITARIOS_H_
#define UTILITARIOS_H_

#include <string>
#include <vector>
using namespace std;

void leerDatoPrototipo(bool espacios=false);
int convertirDigitos(string s);
vector<string> dividir(string cadena, char caracterSeparacion);
string completarConCeros(int numero, int cantidadDigitos);
#endif /* UTILITARIOS_H_ */
