/*
 * servicio.cpp
 *
 *  Created on: Nov 7, 2017
 *      Author: JC
 */

#include "servicio.h"

void ServicioTaxi::inicializar(int codConductor, string nombre, string direccionIni, string direccionDest, int tipoServ,
	string fechaHoraIni, string fechaHoraFin, string fechaHoraReserva, float tarifa){
	codigoConductor = codConductor;
	nombreCliente = nombre;
	direccionInicio = direccionIni;
	direccionDestino = direccionDest;
	tipoServicio = tipoServ;
	tarifaTotal = tarifa;

	Fecha fecTmp;
	Hora horTmp;
	vector<string> vecTmp;

	vecTmp = dividir(string(fechaHoraIni),' ');
	fecTmp.inicializar(vecTmp[0]);
	horTmp.inicializar(vecTmp[1]);
	fechaInicio = fecTmp;
	horaInicio = horTmp;

	vecTmp = dividir(string(fechaHoraFin),' ');
	fecTmp.inicializar(vecTmp[0]);
	horTmp.inicializar(vecTmp[1]);
	fechaFin = fecTmp;
	horaFin = horTmp;

	vecTmp = dividir(string(fechaHoraReserva),' ');
	fecTmp.inicializar(vecTmp[0]);
	horTmp.inicializar(vecTmp[1]);
	fechaReserva = fecTmp;
	horaReserva = horTmp;
}

string ServicioTaxi::convertirCadena(string delimitador){
		stringstream ss;
		string fechaReservaStr = "";
		ss<<codigoConductor<<delimitador<<nombreCliente<<delimitador<<direccionInicio<<delimitador<<
		direccionDestino<<delimitador<<tipoServicio<<delimitador<<fechaInicio.convertirCadena()<<delimitador<<
		horaInicio.convertirCadena()<<delimitador<<fechaFin.convertirCadena()<<delimitador<<horaFin.convertirCadena()<<
		delimitador<<fechaReserva.convertirCadena()<<delimitador<<horaReserva.convertirCadena()<<delimitador<<tarifaTotal;
		return ss.str();
	}

void ServicioTaxi::guardar(string nombreArhivo){
	ofstream archivo(nombreArhivo.c_str(),ios::app);

	if(archivo.is_open()){
		archivo<<this->convertirCadena(",")<<endl;
		archivo.close();
		cout<<"Archivo guardado correctamente"<<endl;
	}else{
		cout<<"Error al abrir archivo"<<endl;
	}

}

