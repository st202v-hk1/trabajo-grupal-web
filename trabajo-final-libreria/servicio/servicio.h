/*
 * servicio.h
 *
 *  Created on: Nov 7, 2017
 *      Author: JC
 */

#ifndef SERVICIO_H_
#define SERVICIO_H_

#include <iostream>
#include <string>
#include "../utilitarios/utilitarios.h"
#include "../utilitarios/fecha-hora/fecha-hora.h"
#include <fstream>

const string ARCHIVO_SERVICIOS_TAXI = "data/servicios.txt";


struct ServicioTaxi{
	int codigoConductor;
	string nombreCliente;
	string direccionInicio;
	string direccionDestino;
	int tipoServicio;
	Fecha fechaInicio;
	Hora horaInicio;
	Fecha fechaFin;
	Hora horaFin;
	Fecha fechaReserva;
	Hora horaReserva;
	float tarifaTotal;


	void inicializar(int codConductor, string nombre, string direccionIni, string direccionDest, int tipoServ,
			string fechaHoraIni, string fechaHoraFin, string fechaHoraReserva, float tarifa);

	string convertirCadena(string delimitador);

	void guardar(string nombreArhivo =ARCHIVO_SERVICIOS_TAXI );
};


typedef struct ServicioTaxi ServicioTaxi;



#endif /* SERVICIO_H_ */
